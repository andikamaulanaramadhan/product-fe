/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}'
  ],
  daisyui: {
    themes: [
      {
        'boostr-light': { /* your theme name */
          primary: '#2463EB', /* Primary color */
          'primary-focus': '#216CA3', /* Primary color - focused */
          'primary-content': '#F9FAFB', /* Foreground content color to use on primary color */

          secondary: '#ffffff', /* Secondary color */
          'secondary-focus': '#d1761c', /* Secondary color - focused */
          'secondary-content': '#2463EB', /* Foreground content color to use on secondary color */

          accent: '#37cdbe', /* Accent color */
          'accent-focus': '#2ea496', /* Accent color - focused */
          'accent-content': '#F9FAFB', /* Foreground content color to use on accent color */

          neutral: '#C53434', /* Neutral color */
          'neutral-focus': '#A72828', /* Neutral color - focused */
          'neutral-content': '#ffffff', /* Foreground content color to use on neutral color */

          'base-100': '#111827', /* Base color of page, used for blank backgrounds */
          'base-200': '#f5f5f5', /* Base color, a little darker */
          'base-300': '#E1E1E1', /* Base color, even more darker */
          'base-content': '#111827', /* Foreground content color to use on base color */

          info: '#58AFFC', /* Info */
          'info-content': '#FF6A3D',

          success: '#27DD70', /* Success */
          'success-content': '#236827', /* Success */

          warning: '#FFCD1C', /* Warning */
          'warning-content': '#c97000', /* Warning */

          error: '#FF5456', /* Error */
          'error-content': '#FFE8DC' /* Error */
        }
      }
    ]
  },
  theme: {
    extend: {
      fontFamily: {
        poppins: 'Poppins'
      },
      colors: {
        primary: '#FF6A3D'
      }
    }
  },
  plugins: [require('daisyui')]
}
