import MainLayout from '@/layout/MainLayout.vue'

const userRoutes = [
  {
    path: '/user',
    component: MainLayout,
    meta: {
      requireAuth: true
    },
    children: [
      {
        path: 'home',
        component: () => import('@/app/page/Home.vue')
      },
      {
        path: 'product',
        component: () => import('@/app/page/Product.vue')
      }
    ]
  }
]

export default userRoutes
