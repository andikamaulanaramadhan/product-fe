import { useLoadingStore } from '@/stores/loading.store'
import { useAuthStore } from '@/stores/auth.store'
import { axios } from '@/plugins/axios'

import tokenLocalStorage from '@/helpers/token'

const middleware = async (to, from, next) => {
  const auth = useAuthStore()
  const loadingStore = useLoadingStore()
  loadingStore.isLoading = true
  const needAuth = to.meta.requireAuth
  const nextPage = (route) => {
    loadingStore.isLoading = false
    next(route)
  }

  const localAuth = tokenLocalStorage.getAuth()
  const hasLocalAuth = Object.keys(localAuth).length > 0

  if (hasLocalAuth) {
    const { token } = localAuth
    auth.setToken(token)
  }

  const isLocalAuthValid = hasLocalAuth && auth.hasLoggedIn

  if (isLocalAuthValid && to.path === '/login') return nextPage('/user/home')

  if ((!auth.hasLoggedIn || !hasLocalAuth) && needAuth) return nextPage('/login')

  if (isLocalAuthValid) {
    try {
      const { data } = await axios.get('user')
      auth.setAuth(data)
      tokenLocalStorage.setUser(data.user)
    } catch (error) {
      auth.unsetAuth()
      tokenLocalStorage.removeAuth()
    }
  }

  nextPage()
}

export default middleware
