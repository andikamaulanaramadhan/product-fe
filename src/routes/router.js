import { createRouter, createWebHistory } from 'vue-router'

import guardRoutes from './guard'
import userRoutes from './user'
import middleware from './middleware'

const routes = [
  ...guardRoutes,
  ...userRoutes
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  middleware(to, from, next)
})
export default router
