const guardRoutes = [
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('@/app/page/NotFound.vue')
  },
  {
    path: '/',
    component: () => import('@/app/page/LandingPage.vue')
  },
  {
    path: '/login',
    component: () => import('@/app/page/Login.vue')
  }
]

export default guardRoutes
