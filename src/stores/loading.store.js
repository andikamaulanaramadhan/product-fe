import { defineStore } from 'pinia'

import { ref } from 'vue'

export const useLoadingStore = defineStore('useLoadingStore', () => {
  const isLoading = ref(false)
  return {
    isLoading
  }
})
