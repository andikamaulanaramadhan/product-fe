import { defineStore } from 'pinia'

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    user: null,
    token: null,
    hasLoggedIn: false
  }),
  actions: {
    getToken () {
      return this.token
    },
    getUser () {
      return this.user
    },
    setToken (token) {
      this.token = token
      this.hasLoggedIn = true
    },
    setUser (user) {
      this.user = user
    },
    setAuth ({ user, token }) {
      this.user = user
      this.token = token
      this.hasLoggedIn = true
    },
    unsetAuth () {
      this.user = null
      this.token = null
      this.hasLoggedIn = false
    }
  }
})
