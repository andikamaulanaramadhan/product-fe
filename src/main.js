import { createApp } from 'vue'
import App from './App.vue'
import router from '@/routes/router'
import { createPinia } from 'pinia'
import { storage } from '@/firebase/storage-firebase'
import './style.css'
import 'bootstrap-icons/font/bootstrap-icons.css'

const app = createApp(App)
const pinia = createPinia()
app.config.globalProperties.$storage = storage

app.use(pinia)
app.use(router)
app.mount('#app')
