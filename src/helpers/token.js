class TokenLocalStorage {
  setAuth ({ user, token }) {
    const auth = JSON.stringify({ user, token })
    localStorage.setItem('_auth', auth)
  }

  getAuth () {
    const auth = JSON.parse(localStorage.getItem('_auth') || '{}')
    return auth
  }

  setToken (token) {
    const auth = JSON.parse(localStorage.getItem('_auth') || '{}')

    auth.token = token

    localStorage.setItem('_auth', JSON.stringify(auth))
  }

  removeAuth () {
    localStorage.removeItem('_auth')
  }

  setUser (user) {
    const auth = JSON.parse(localStorage.getItem('_auth'))
    auth.user = user

    localStorage.setItem('_auth', JSON.stringify(auth))
  }
}

export default new TokenLocalStorage()
