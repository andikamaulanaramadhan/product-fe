export function openModal (idModal) {
  document.getElementById(idModal).showModal()
}

export function closeModal (idModal) {
  document.getElementById(idModal).close()
}
