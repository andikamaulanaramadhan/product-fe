import { initializeApp } from 'firebase/app'
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyCUUSNVSex_aIFNKhyxB6WxFSfDTW3jUh8',
  authDomain: 'fir-1bb7d.firebaseapp.com',
  projectId: 'fir-1bb7d',
  storageBucket: 'fir-1bb7d.appspot.com',
  messagingSenderId: '444034037080',
  appId: '1:444034037080:web:ca3958215fa7b3e4d1e12d',
  measurementId: 'G-HXWK5X14Q5'
}

const firebaseApp = initializeApp(firebaseConfig)
const storage = getStorage(firebaseApp)

export { storage }
