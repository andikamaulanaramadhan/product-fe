const sidebarItems = [
  {
    icon: 'bi bi-house-door-fill',
    title: 'Home',
    path: '/user/home'
  },
  {
    icon: 'bi bi-kanban',
    title: 'Product',
    path: '/user/product'
  }
]

export default sidebarItems
