import pluginAxios from 'axios'
import localStorageToken from '@/helpers/token.js'
import { useAuthStore } from '@/stores/auth.store.js'

let axios = document.axios

if (!axios) {
  document.requestTokenState = 'done'
  document.axios = pluginAxios.create()
  document.axios.defaults.baseURL = import.meta.env.VITE_AXIOS_URL
  document.axios.defaults.headers['Content-Type'] = 'application/json'

  document.axios.interceptors.request.use(
    (config) => {
      if (config.data instanceof FormData) {
        config.headers['Content-Type'] = 'multipart/form-data'
      }
      const { token } = localStorageToken.getAuth()
      if (token) {
        const auth = useAuthStore()
        auth.setToken(token)
        config.headers.Authorization = 'Bearer ' + token // for Spring Boot back-end
      }
      return config
    },
    (error) => {
      return Promise.reject(error)
    }
  )

  axios = document.axios
}

export { axios }
