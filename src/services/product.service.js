import { axios } from '@/plugins/axios'

export function getProduct (params) {
  return axios.get('product', { params })
}

export function deleteProduct (id) {
  return axios.delete(`product/${id}`)
}

export function editProduct (id, payload) {
  return axios.put(`product/${id}`, { payload })
}

export function addProduct ({ image, name, category, stock, price }) {
  return axios.post('product', { image, name, category, stock, price })
}
