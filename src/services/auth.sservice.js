import { axios } from '@/plugins/axios'

export function login (form) {
  return axios.post('auth/login', form)
}
